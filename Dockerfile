#FROM node:7.5.0
#FROM emeraldsquad/alpine-scripting
FROM ruby:2.6-alpine3.11
LABEL maintainer="Simon V. Laursen <svl@gavdilabs.com>"
LABEL version="1.0"

ENV PACKAGES "unzip curl openssl ca-certificates git libc6-compat bash jq gettext make openssh nodejs npm python sqlite-dev chromium"

#CF CLI Version - Change if you want a different version
ENV CF_CLI_VERSION "6.50.0" 

#CF Multiapps Plugin Binary File
ENV CF_MTA=multiapps-plugin.linux32

# Chrome Environment Variable - Karma Tests
ENV CHROME_BIN /usr/bin/chromium-browser

#Prepare Image
RUN apk update
RUN apk add --no-cache $PACKAGES

# Install CF CLI
RUN wget -q -O - 'https://cli.run.pivotal.io/stable?release=linux64-binary&source=github&version='${CF_CLI_VERSION} \
  | tar -xzf - -C /usr/bin

# Install CF CLI Plugins
ADD plugins .

RUN cf install-plugin -f ${CF_MTA} \
    && cf plugins \
    && rm -f ${CF_MTA}

# Run Command Line
CMD /bin/bash

# Install Essential Node Packages
#RUN npm install -g @sap/cds
RUN npm install -g karma-cli
RUN npm install -g @ui5/uiveri5