![Header](./res/header.png)

# Cloud Foundry NodeJS CI/CD - Docker Image

This Dockerfile is made for projects running CI/CD with **SAP CAPIRE NodeJS**. 

If there is a tool missing from the image that you need for your CI/CD process, please let me know. However, keep in mind that the goal is to keep this image as light as possible in order keep processing times down.

**The current version of the image includes:**

- [Cloud Foundry CLI](https://github.com/cloudfoundry/cli)
- [Cloud Foundry MultiApps Plugin](https://github.com/cloudfoundry-incubator/multiapps-cli-plugin)
- [Node JS](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/) 
- [Git](https://git-scm.com/)
- [Karma CLI](https://github.com/karma-runner/karma-cli) 
- [UiVeri5 CLI](https://github.com/SAP/ui5-uiveri5)
- [Chromium](https://www.chromium.org/)
- [Bash](https://www.gnu.org/software/bash/)

